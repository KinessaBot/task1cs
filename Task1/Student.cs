﻿using System;

namespace Task1
{
    class Student
    {
        Mark[] marks;
        public String name { get; set; }
        int markLength = 0;

        // parametrs for task 2 
        public static decimal basicGrants {get; set;} //property example
        const string university = "Gomel State University"; //constant example
        const int maxRetakes = 3;
        int retakeCount = 0;   //private field
        uint grantsMulti = 0;  //classic field example

        public Student(string name)
        {
            this.name = name;
            marks = new Mark[10];
        }

        public float GetAvgMark()
        {
            if (marks.Length == 0)
                throw new DivideByZeroException();
            float result = 0f;
            return MarkSum()/markLength;
        }

        public void ResetAllMarks()
        {
            for (int i = 0; i < markLength; i++)
            {
                marks[i].mark=0;
            }
            retakeCount = 0;            // clear retakes
        }

        public void AddMark(int mark, string nameOfSubj)
        {
            if (marks.Length == markLength)
                Array.Resize<Mark>(ref marks, markLength * 2);
            marks[markLength] = new Mark(mark, nameOfSubj);
            if (mark < 4)              // exam result validation
                retakeCount++;
            markLength++;
        }

        //statements for task 2 

        public bool Allocation()
        {
            return !(retakeCount < Student.maxRetakes);
        }

        static public bool HasMark(Student student, string nameOfSubj)  //static statement example
        {
            for (int i = 0; i < student.markLength; i++)
                if (String.Compare(student.marks[i].nameOfSubj, nameOfSubj) == 0)
                    return true;
            return false;
        }

        public decimal GetGrants()
        {
            return basicGrants * grantsMulti;
        }

        public void SetGrantsMulti(uint multi)
        {
            grantsMulti = multi;
        }

        public uint GetGrantsMulti()
        {
            return grantsMulti;
        }

        public string Info()
        {
            return name + " is a student of " + university + ", has " + retakeCount + " retakes in this term.";
        }

        private int MarkSum()   // private function example
        {
            int result = 0; 
            for (int i = 0; i < markLength; i++)
                result += marks[i].mark;
            return result;
        }


    }
}
