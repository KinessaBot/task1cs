﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task1
{
    class Program
    {
        public static void Main()
        {
            Student Ivan = new Student("Ivan");
            Ivan.AddMark(10, "Sport");
            Ivan.AddMark(3, "ML");
            Ivan.AddMark(7, "English");
            Student.basicGrants = 100M;
            Ivan.SetGrantsMulti(1);
            Console.WriteLine(Ivan.GetAvgMark());
            Console.WriteLine(Ivan.Info());
            Console.WriteLine(Ivan.GetGrants() + " Grants");
            Console.WriteLine("__________");
            Ivan.ResetAllMarks();
            Console.WriteLine(Ivan.GetAvgMark());
            Console.WriteLine(Ivan.Info());
            Console.ReadLine();
        }
    }
}
