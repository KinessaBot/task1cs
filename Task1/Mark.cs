﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task1
{
    class Mark
    {
        public int mark {get; set;}
        public string nameOfSubj { get; set; }

        public Mark(int mark, string nameOfSubj)
        {
            this.mark = mark;
            this.nameOfSubj = nameOfSubj;
        }
    }
}
